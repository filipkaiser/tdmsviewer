﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10197915</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">2789375</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&amp;!!!*Q(C=\&gt;5R&lt;NN!%)8BFS"&amp;7O9%A5KHH#MI:9I5)J!,T"85OF3L&gt;(-&amp;&gt;H(,%Q39+_A+OA,T\WIE/Y&amp;B.&lt;%2"/:K&lt;?FR&gt;`&lt;TEK+F@LS8XGGZ&gt;IR0(JRP1Z&lt;7FF-@\V`^Z,GVVXA:?YI?TP`N`,D=FXBM`PF4T2``[-NY,H(6@_81[`&amp;[`.`(\&gt;5PQ&lt;.`$^[I09DI31_[UUVNW38*ETT*ETT*ETT)ATT)ATT)A^T*H&gt;T*H&gt;T*H&gt;T)D&gt;T)D&gt;T)D&lt;QX=J',8/31EM74B:+C39&amp;E-"1F0R*0YEE]C9?03DS**`%EHM4$%#7?R*.Y%E`C9:I34_**0)EH]6#K3\)X=DS*B`)+0)%H]!3?Q-/3#DQ")&amp;AM+"Q5A;(A:0!G]!3?Q-.&lt;":\!%XA#4_$BN!*0Y!E]A3@Q-+8P3H2.;_2Y+#0(YXA=D_.R0*37YX%]DM@R/"[7E_.R0!\#7&gt;!J$E(/*'?!]](R/"Z_S@%Y(M@D?"Q0J`I6]LYT4&gt;-;/2\$9XA-D_%R0*31Y4%]BM@Q'"\+SP!9(M.D?!Q03]HQ'"\$9U#-26F?2D&amp;DID()#!Q0LX[X7,^+U387GV1XL_KG6.VMKJN)&gt;8/I,LLK9KIOEGLT6:OKWCT6*KD_/"6;B6%NIJL="OL)TQ.^JE`U(8V,X^$8^"6^;&amp;0`]M$D];D$Y;"ZHD6.EX;\H&lt;&lt;&lt;L4;&lt;D&gt;&lt;LN6;LF9:BO$Q'0N)O$Y44=_G7^X@T^S`4`N08O`X.TWF`]`H(]/&amp;&lt;SV`A``-`]'T57TV=ATX["5$M?W9!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Buttons</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="About.ctl" Type="VI" URL="../About.ctl"/>
	<Item Name="Bock.ctl" Type="VI" URL="../Bock.ctl"/>
	<Item Name="ConvertTDMS.ctl" Type="VI" URL="../ConvertTDMS.ctl"/>
	<Item Name="Exit.ctl" Type="VI" URL="../Exit.ctl"/>
	<Item Name="Help.ctl" Type="VI" URL="../Help.ctl"/>
	<Item Name="Minimize.ctl" Type="VI" URL="../Minimize.ctl"/>
	<Item Name="OpenFilepath.ctl" Type="VI" URL="../OpenFilepath.ctl"/>
	<Item Name="OpenTDMS.ctl" Type="VI" URL="../OpenTDMS.ctl"/>
</Library>
